import numpy as np
import pandas as pd
import pickle
import os
from functools import partial

from pandas_ods_reader import read_ods


def import_food_data(lang):
    if lang not in ["en", "dk"]:
        raise ValueError("Database language must be English or Danish")
    return read_ods("frida/frida-{}.ods".format(lang), 2)


def import_pickle():
    with open("food_data.pkl", "rb") as pickle_file:
        return pickle.load(pickle_file)


def search_by_name(food_name, food_data):
    index_list = []
    for ii in range(len(list(food_data["Name"]))):
        name = list(food_data["Name"])[ii]
        if type(name) == str:
            if food_name.casefold() in name.casefold():
                index_list.append(ii)
    return food_data.loc[index_list, "Name"]


class FoodDatabase:
    def __init__(self, lang="en"):
        if os.path.isfile("food_data.pkl"):
            self.food_data = import_pickle()
        else:
            self.food_data = import_food_data(lang)
            with open("food_data.pkl", "wb") as pickle_file:
                pickle.dump(self.food_data, pickle_file)

    def search(self, food_name):
        return search_by_name(food_name, food_data=self.food_data)

    def entry_name(self, entry_num):
        return self.food_data.loc[entry_num, "Name"]

    def add_value(self, index, column, amount):
        value = self.food_data.loc[index][column]
        if value == "nv":
            return 0
        else:
            return value * amount / 100

    def ingredient_series(self, index, used_columns=None):
        """return a series from the food_data for one ingredient
        used_columns can be specified to ignore the remaining columns"""
        if used_columns is None:
            return self.food_data.loc[index]
        data = [self.food_data.loc[index, column] for column in used_columns]
        data = [0 if type(x) == str else x for x in data]
        return pd.Series(data, index=used_columns)


if __name__ == "__main__":
    if os.path.isfile("food_data.pkl"):
        food_data = import_pickle()
    else:
        food_data = import_food_data("en")
    search = partial(search_by_name, food_data=food_data)
