import jinja2
import yaml
import re
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl

import frida

mplparams = {"font.size": "7"}
mpl.rcParams.update(mplparams)


metric_mass_dict = {"mg": 0.001, "g": 1, "kg": 1000}
metric_vol_dict = {
    "mL": 1,
    "ml": 1,
    "cL": 10,
    "cl": 10,
    "dL": 100,
    "dl": 100,
    "L": 1000,
    "tsp": 5,
    "tbsp": 15,
}
imperial_mass_dict = {
    "oz": 28.375,
    " oz": 28.375,
    "lb": 454,
    " lb": 454,
    "lbs": 454,
    " lbs": 454,
}
imperial_vol_dict = {
    "oz": 28.375,
    " oz": 28.375,
    "fl oz": 28.375,
    " fl oz": 28.375,
    "cups": 240,
    " cups": 240,
    "cup": 240,
    " cup": 240,
    "pt": 480,
    " pt": 480,
    "pint": 480,
    " pint": 480,
    "qt": 960,
    " qt": 960,
    "quart": 960,
    " quart": 960,
    "gal": 1920,
    " gal": 1920,
    "gallon": 1920,
    " gallon": 1920,
}
danish_unit_dict = {"tsk": 5, "spsk": 15}

mass_dict = {**metric_mass_dict, **imperial_mass_dict}
vol_dict = {**metric_vol_dict, **imperial_vol_dict, **danish_unit_dict}

currency_string_dict = {
    "DKK": "{:.2f}kr.",
    "SEK": "{:.2f}kr.",
    "NOK": "{:.2f}kr.",
    "EUR": "€{:.2f}",
    "USD": "${:.2f}",
    "GBP": "£{:.2f}",
}


class UserProfile:
    def __init__(self, profile_name):
        profile_filename = "profiles/{}.yml".format(profile_name)
        with open(profile_filename, "r") as stream:
            self.profile_dict = yaml.safe_load(stream)
        self.recipe_template_filename = "templates/{}.txt".format(
            self.profile_dict["latex"]["recipe template"]
        )
        self.main_template_filename = "templates/{}.txt".format(
            self.profile_dict["latex"]["main template"]
        )
        self.currency = self.profile_dict["localization"]["currency"]
        self.cost_string = currency_string_dict[self.currency]

        self.nutrition_names_display = self.profile_dict["database"]["nutrition list"]
        self.nutrition_names_amino = self.profile_dict["database"]["amino acids"]
        self.nutrition_names_all = dict()
        self.nutrition_names_all.update(self.nutrition_names_display)
        self.nutrition_names_all.update(self.nutrition_names_amino)
        self.show_amino = self.profile_dict["latex"]["show_amino"]
        if self.profile_dict["database"]["source"] == "frida":
            self.database = frida.FoodDatabase()
        else:
            raise Exception("No databases besides frida implemented yet")
        self.ingredients_database = self.profile_dict["ingredients"]["database"]
        self.ingredients_recipes = self.profile_dict["ingredients"]["recipe"]
        self.recipes_main = dict()
        self.recipes_dependency = dict()
        self.reference_ingredient = Ingredient(
            self.profile_dict["reference ingredient"], 100, self
        )

        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader("."),
            variable_start_string="(%",
            variable_end_string="%)",
            trim_blocks=True,
            lstrip_blocks=True,
        )
        self.template_recipe = env.get_template(self.recipe_template_filename)
        self.template_main = env.get_template(self.main_template_filename)
        self.cookbook_recipe_list = self.profile_dict["recipes"]
        self.make_dependencies = self.profile_dict["latex"]["make dependencies"]
        self.unresolved_list = []
        for recipe in self.cookbook_recipe_list:
            self.add_recipe(recipe)
        self.gen_main_tex()

    def add_recipe(self, recipe_namebase):
        self.unresolved_list.append(recipe_namebase)
        self.recipes_main[recipe_namebase] = Recipe(recipe_namebase, self)
        self.gen_recipe_tex(self.recipes_main[recipe_namebase])
        self.unresolved_list.pop(self.unresolved_list.index(recipe_namebase))
        return self.recipes_main[recipe_namebase]

    def add_dependency(self, recipe_namebase):
        self.unresolved_list.append(recipe_namebase)
        self.recipes_dependency[recipe_namebase] = Recipe(recipe_namebase, self, True)
        self.gen_recipe_tex(self.recipes_dependency[recipe_namebase])
        self.unresolved_list.pop(self.unresolved_list.index(recipe_namebase))
        return self.recipes_dependency[recipe_namebase]

    def gen_recipe_tex(self, recipe):
        wr_out = self.template_recipe.render(recipe=recipe)
        with open("latex/{}.tex".format(recipe.namebase), "w") as f_out:
            f_out.write(wr_out)

    def gen_main_tex(self):
        wr_out = self.template_main.render(profile=self)
        with open("latex/main.tex", "w") as f_out:
            f_out.write(wr_out)


class Recipe:
    def __init__(self, namebase, profile, dependency=False):
        """Recipe object should initialize dicts of ingredient and preparation
        objects and strings respectively.

        @param str namebase: recipe filename without path or extension
        @param obj profile: UserProfile object
        @param bool dependency: track whether recipe is called for an 
        ingredient in another recipe
        """
        self.namebase = namebase
        self.profile = profile
        self.is_dependency = dependency
        self.cost_string = profile.cost_string

        recipe_filename = "recipes/{}.yml".format(namebase)
        with open(recipe_filename, "r") as stream:
            self.recipe_dict = yaml.safe_load(stream)

        self.name = self.recipe_dict["metadata"]["name"]
        self.portions = self.recipe_dict["metadata"]["portion"]
        self.source = self.recipe_dict["metadata"]["source"]
        self.dependencies_list = self.recipe_dict["dependencies"]
        if self.dependencies_list is None:
            self.dependencies_list = []

        if type(self.recipe_dict["preparation"]) == dict:
            self.preparation = self.recipe_dict["preparation"]
        else:
            self.preparation = dict()
            self.preparation["uncategorized"] = self.recipe_dict["preparation"]

        self.nutrition_series_list = []
        self.cost = 0
        self.grams = 0
        self.ingredients = self.group_dict(self.recipe_dict["ingredients"])
        for category in self.ingredients:
            self.ingredients[category] = self.dict_to_ing_list(
                self.ingredients[category]
            )
            for ing in self.ingredients[category]:
                self.nutrition_series_list.append(ing.nutrition_series)
                self.cost += ing.cost_per_kilo * ing.grams / 1000
                self.grams += ing.grams

        self.nutrition_array = pd.DataFrame(
            self.nutrition_series_list,
            columns=list(self.profile.nutrition_names_all.keys()),
        )
        self.nutrition_dict_display = dict()
        self.nutrition_dict_amino = dict()
        self.nutrition_dict_all = dict()
        for name in self.nutrition_array.columns:
            if name in self.profile.nutrition_names_amino:
                self.nutrition_dict_amino[
                    self.profile.nutrition_names_amino[name]
                ] = sum(self.nutrition_array[name])
            if name in self.profile.nutrition_names_display:
                self.nutrition_dict_display[
                    self.profile.nutrition_names_display[name]
                ] = sum(self.nutrition_array[name])
            self.nutrition_dict_all[self.profile.nutrition_names_all[name]] = sum(
                self.nutrition_array[name]
            )
        for dependency in self.dependencies_list:
            self.profile.add_dependency(dependency)
        self.amino_acid_plotter = AminoAcidComparison(
            self, self.profile.reference_ingredient
        )

    def dict_to_ing_list(self, ingredient_dict):
        ing_list = []
        for key in ingredient_dict.keys():
            if key in self.profile.ingredients_recipes:
                # Check for circular dependencies
                if (
                    self.profile.ingredients_recipes[key]
                    in self.profile.unresolved_list
                ):
                    raise Exception(
                        "Circular dependencies with {} and {}".format(
                            self.profile.ingredients_recipes[key], self.namebase
                        )
                    )
                # Remove dependencies from list which are already made as ingredients
                if self.profile.ingredients_recipes[key] in self.dependencies_list:
                    self.dependencies_list.pop(
                        self.dependencies_list.index(
                            self.profile.ingredients_recipes[key]
                        )
                    )
            ing_list.append(Ingredient(key, ingredient_dict[key], self.profile))
        return ing_list

    def group_dict(self, ungrouped_dict):
        first_levels = list(ungrouped_dict.keys())
        typelist = [type(ungrouped_dict[x]) for x in first_levels]
        uncategorized_indices = [i for i, t in enumerate(typelist) if not t == dict]
        grouped_dict = dict()
        ii = 0
        while ii < len(first_levels):
            if ii in uncategorized_indices:
                jj = ii + 1
                while jj in uncategorized_indices:
                    jj += 1
                grouped_dict["uncategorized{}".format(ii)] = dict(
                    [
                        [first_levels[k], ungrouped_dict[first_levels[k]]]
                        for k in range(ii, jj)
                    ]
                )
                ii = jj
            else:
                grouped_dict[first_levels[ii]] = ungrouped_dict[first_levels[ii]]
                ii += 1
        if "uncategorized" in list(grouped_dict.keys())[0]:
            newdict = dict()
            newdict["uncategorized_first"] = grouped_dict[list(grouped_dict.keys())[0]]
            for ii in range(len(grouped_dict.keys()) - 1):
                newdict[list(grouped_dict.keys())[ii + 1]] = grouped_dict[
                    list(grouped_dict.keys())[ii + 1]
                ]
            grouped_dict = newdict
        return grouped_dict


class Ingredient:
    def __init__(self, name, quantity, profile):
        self.name = name
        self.profile = profile
        self.quantity = quantity
        self.density = 1
        self.grams = self.get_grams_of()
        if self.name in profile.ingredients_database.keys():
            self.index = profile.ingredients_database[self.name][0]
            self.cost_per_kilo = profile.ingredients_database[self.name][1]
            self.database_name = (
                profile.database.entry_name(self.index)
                .replace("_", "\_")
                .replace("%", "\%")
                .replace("`", "'")
            )
            self.nutrition_series = (
                profile.database.ingredient_series(
                    self.index, used_columns=list(profile.nutrition_names_all.keys())
                )
                * self.grams
                / 100
            )
        else:
            self.index = "local"
            if self.name in profile.ingredients_recipes:
                if self.name in profile.cookbook_recipe_list:
                    if profile.ingredients_recipes[self.name] in profile.recipes_main:
                        dependency = profile.recipes_main[
                            profile.ingredients_recipes[self.name]
                        ]
                    else:
                        dependency = profile.add_recipe(
                            profile.ingredients_recipes[self.name]
                        )
                else:
                    if (
                        profile.ingredients_recipes[self.name]
                        in profile.recipes_dependency
                    ):
                        dependency = profile.recipes_dependency[
                            profile.ingredients_recipes[self.name]
                        ]
                    else:
                        dependency = profile.add_dependency(
                            profile.ingredients_recipes[self.name]
                        )
                self.nutrition_series = (
                    pd.Series(
                        list(dependency.nutrition_dict_all.values()),
                        index=list(profile.nutrition_names_all.keys()),
                    )
                    / dependency.grams
                    * self.grams
                )
                self.cost_per_kilo = dependency.cost / dependency.grams * 1000
                self.database_name = profile.ingredients_recipes[self.name]
            else:
                self.cost_per_kilo = 0
                self.database_name = "UNDEFINED"
                self.nutrition_series = (
                    pd.Series(
                        np.zeros(len(profile.nutrition_names_all)),
                        index=list(profile.nutrition_names_all.keys()),
                    )
                    * self.grams
                    / 100
                )

    def get_grams_of(self):
        if type(self.quantity) == int:
            if self.quantity == 0:
                self.quantity = ""
                return 0
            grams = self.quantity
            self.quantity = str(grams) + "g"
            return grams
        elif type(self.quantity) == str:
            for unit in mass_dict:
                if re.match("\d+x".replace("x", unit), self.quantity):
                    return float(self.quantity.replace(unit, "")) * mass_dict[unit]
            for unit in vol_dict:
                if re.match("\d+x".replace("x", unit), self.quantity):
                    return (
                        float(self.quantity.replace(unit, ""))
                        * vol_dict[unit]
                        * self.density
                    )


class AminoAcidComparison:
    def __init__(self, main, reference=None):
        self.main = main
        self.reference = reference
        if type(main) == Recipe:
            amino_dict = main.nutrition_dict_amino.copy()
        elif type(main) == Ingredient:
            amino_dict = dict()
            for acid in main.profile.nutrition_names_amino.keys():
                amino_dict[acid] = main.nutrition_series[acid]
        else:
            raise Exception(
                "Only Ingredient or Recipe objects can be passed to AminoAcidComparison"
            )
        if reference is not None:
            if type(reference) == Recipe:
                for acid in amino_dict.keys():
                    amino_dict[acid] = [
                        amino_dict[acid],
                        reference.nutrition_dict_amino[acid],
                    ]
            elif type(reference) == Ingredient:
                for acid in amino_dict.keys():
                    amino_dict[acid] = [
                        amino_dict[acid],
                        reference.nutrition_series[acid],
                    ]
            else:
                raise Exception(
                    "Only Ingredient or Recipe objects can be passed to"
                    "AminoAcidComparison"
                )
        else:
            for acid in amino_dict.keys():
                amino_dict[acid] = [amino_dict[acid]]
        self.dataframe = pd.DataFrame.from_dict(amino_dict)
        for ii in range(len(self.dataframe)):
            self.dataframe.loc[ii] /= sum(self.dataframe.loc[ii])
        self.dataframe = self.dataframe.transpose()
        for ii in range(len(self.dataframe)):
            self.dataframe.loc[self.dataframe.index[ii]][0] = (
                self.dataframe.loc[self.dataframe.index[ii]][0]
                * 200
                / (
                    self.dataframe.loc[self.dataframe.index[ii]][0]
                    + self.dataframe.loc[self.dataframe.index[ii]][1]
                )
            )
            self.dataframe.loc[self.dataframe.index[ii]][1] = (
                200 - self.dataframe.loc[self.dataframe.index[ii]][0]
            )
        if reference is not None:
            self.make_plot()

    def make_plot(self):
        self.figure, self.ax = plt.subplots(figsize=(3, 5), constrained_layout=True)
        cmap = sns.diverging_palette(16, 250, as_cmap=True)
        bars = self.ax.barh(
            self.dataframe.index,
            self.dataframe[0],
            label=self.dataframe.index,
            color=cmap(self.dataframe[0] / 200),
        )
        self.ax.barh(
            self.dataframe.index,
            self.dataframe[1],
            left=self.dataframe[0],
            color=cmap(self.dataframe[1] / 200),
        )
        for bar in bars:
            yloc = bar.get_y() + bar.get_height() / 2
            self.ax.annotate(
                "{:d}%".format(int(bar.get_width())),
                xy=(195, yloc),
                xytext=(-5, 0),
                textcoords="offset points",
                ha="right",
                va="center",
                color="k",
                weight="bold",
                clip_on=True,
            )
        if type(self.main) == Recipe:
            self.figure.savefig("latex/figs/{}.pdf".format(self.main.namebase))


if __name__ == "__main__":
    default = UserProfile("default")
