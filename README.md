# freelunch

freelunch is a free software project to keep track of recipes and nutrition in a well-formatted cookbook.
It is designed to be highly modular. Users can add/change recipes, LaTeX templates, and even sources of nutrition information.
Recipes are stored as yml files, where preparation steps and ingredients are listed, and this software handles the rest.
See latex/main.pdf for an example with my rugbrød recipe.

This is a very early stage of the project. Currently, it is up to the user to handle the dependencies and compiling the LaTeX document.

# Goals

The aim of this project is to have a standardized way for storing and sharing recipes.
With myriad websites offering printable forms of their recipes and (sometimes sparse or absent) nutrition information, one must manually copy each recipe into their own system in order to have a unified repository.
Instead, recipes could be shared simply as a list of ingredients and preparation steps, and each user can seamlessly add them to their own cookbook with whatever information they wish automatically derived from the ingredients.

This software could enable the creation of a public recipe repository as a substitute for the common practice of accessing recipes via ad-ridden user-tracking sites.
The repository could be community-organized, with volunteer maintainers packaging collections of user-submitted recipes in a format that could be easily distributed for use with this software as a frontend.
Being designed modularly, the community would be free to develop other frontends and to extend features of this project.

I was motivated in the first place by wanting to optimize my normal weekday meals around cost and preparation time per nutritional value. I'm also interested in developing a plant-based diet that's high in protein with a satisfactory amino acid profile and answering whether it is close to the optimum of cost and time, which requires more sophisticated analysis tools than I could find freely.

# Features to add

- Recipe sorting (e.g. by protein/cost, vitamins/kcal, etc.)
- Mobile-friendly LaTeX templates
- Nutrition information for week-long diet plans
- Support for imperial units (though you really ought not use imperial units.)
- Potentially other sources of nutrition data
- If less tech-savvy users are intimidated (and this proves useful to enough people), I could eventually add a GUI for creating the recipes and generating the cookbooks.

# Dependencies

- Python 3.7 (necessary to guarantee the order of entries in a dict)
- jinja2
- numpy
- pandas

``pip install jinja2 numpy pandas``

- A LaTeX installation (I recommend TexLive).

Currently, my template doesn't include packages outside of the base LaTeX.

# Use

Use recipes/example.yml as a guide to constructing recipe files, and store them all in recipes/.
Refer to them by their filenames without the extension in the profile.

Either modify profiles/default.yml or copy it to e.g. profiles/myprofile.yml and have core.py use 'myprofile' instead.

**NB: you must use spaces, not tabs, in the yml files.**

You can interactively find ingredients in the frida database by running
```
python frida.py
```
```python
search('milk')
```
Then when it returns a list of entries containing the word 'milk', write the index of the entry you prefer into the profile.

Once ready, run
```
python freelunch.py
cd latex
latexmk main.tex
```

Note that ``python`` must execute python 3. In some systems, you will need to replace this with ``python3``

# Amino acid plots

Currently, the plots I've implemented show the fraction of the amino acid present in the recipe relative to the fraction of that amino acid present in a reference ingredient (which is set in the profile).
This means that if an amino acid accounts for 3% of the total in the recipe and 6% of the total in the reference, then 50% will be displayed.
If the relative quantity of all amino acids in the recipe closely corresponds to that of the reference ingredent, then near 100% will be displayed for all amino acids.
It is important to note that this does not show the quantity of one amino acid relative to another, and thus may not be the most salient way of comparing amino acid values.
This is a start, which should at least reveal if there are any large deficiencies, especially in the context of a feature I plan to add whereby the nutrition value over a week's diet can be considered.

# Known issues

I haven't implemented a sensible way of handling ingredients that should be given in arbitrary units, e.g. cloves of garlic, stalks of celery.
What may be necessary is a file full of rules to consult where each individual ingredient is given a default conversion from unspecified units into grams.
If nothing better can be implemented, then it would be necessary to fill this out over time as the users encounter un-ruled ingredients.

I'm not sure how to best handle conversion to mass when ingredients are given in volume units, since the database I currently use doesn't have density data for most ingredients.
Currently, I assume only ingredients with 1g/mL density (i.e. water and, with some small error, liquids and oil in general) are given volumentrically.
*NB: Measuring flour by volume hinders sharing of recipes, because density varies quite a lot, and it is the relative mass of ingredients that determines baking outcomes*

When calculating nutrition and cost per weight, this assumes that the weight of the final result is equal to the sum of the weight of the ingredients.
This holds roughly true when baking, but when one makes rice for example, the weight of the water isn't typically counted as an ingredient, and when it is, the amount that evaporates away is certainly missed. I'm not sure how best to handle that, but just bear this in mind when interpreting the results.

# Database

The database I've currently implemented is from the National Food Institute at the Technical University of Denmark and can be downloaded [here](https://frida.fooddata.dk/?lang=en).
Though it is free, public data, it is not yet licensed in such a way that users can redistribute it, though it is possible that it will become so.
For now, you must download it and move the english ods spreadsheet to frida/frida-en.ods.
I've been in contact with the database maintainers who indicated that there will likely be an API from around December that will be remotely accessible at runtime of the code, meaning that manually downloading it won't be necessary from then.

This database is curated in Denmark with ingredients that reflect what is commonly found here.
It lacks some common ingredients and is not perfect for international use, but it does have detailed information about the amino acid profiles for each food, which is very rare in such databases.
What I'll most likely add in the near term is a way to supplement frida with other data. One could manually copy an ingredient or two from a more fleshed-out proprietary database like nutritiondata.self.com, but I won't get into the legal issues of distributing that for now.

Food data (frida.fooddata.dk), version 4, 2019, National Food Institute, Technical University of Denmark
Food data made available by National Food Institute, Technical University of Denmark (frida.fooddata.dk)

